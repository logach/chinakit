/**
 * Created by logach on 4/7/14.
 */

$(document).ready(function(){

    var venueAddress = "Ворошиловский просп., 28, Ростов-на-Дону, Ростовская область";
    var markerInfo = "<h4>China Kit</h4>";

    $("#map_canvas").gmap3({
            map: {
                options: {
                    maxZoom: 15,
                    streetViewControl: false,
                    panControl: true,
                    scrollwheel: false,
                    panControlOptions: {
                        position: google.maps.ControlPosition.RIGHT_CENTER
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    mapTypeControl: false

                }
            },
            infowindow: {
                address: venueAddress,
                options: {
                    content: markerInfo
                }
            },
            marker: {
                address: venueAddress
            }
        },
        "autofit");

})
